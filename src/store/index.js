import { createStore } from 'vuex';

import membersModules from './modules/members/index.js';
import eventModules from './modules/events/index.js';
import authModule from './modules/auth/index.js';

const store = createStore({
    modules: {
        members: membersModules,
        events: eventModules,
        auth: authModule
    }
});

export default store;