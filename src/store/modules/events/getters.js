export default {
    events(state, _, _2, rootGetters){
        const userId = rootGetters.userId;
        //console.log(userId);
        return state.events.filter(event => event.userId === userId);
    },
    hasEvents(_, getters){
        return getters.events && getters.events.length > 0;
    }


}