// eventName: '',
// eventDate: '',
// eventDesc: '',
// userId: this.$route.id
export default {
    async createEvent(context, payload){
        //const userId = payload.userId;
        const eventData = {
            eventName: payload.eventName,
            eventDate: payload.eventDate,
            eventDesc: payload.eventDesc
        };
        const response = await fetch(
            `https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/events/${payload.userId}.json`,
            {
                method: 'POST',
                body: JSON.stringify(eventData)
            });

            const responseData = await response.json();

            if(!response.ok){
                //error
            }

            eventData.id = responseData.name;
            eventData.userId = payload.userId;
            context.commit('setEvents', eventData);
        },
        async fetchEvents(context) {
            const userId = context.rootGetters.userId;
            const response = await fetch(`https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/events/${userId}.json`);
            const responseData = await response.json();
        
            if (!response.ok) {
              const error = new Error(responseData.message || 'Failed to fetch requests.');
              throw error;
            }
        
            const events = [];

            // console.log(responseData);
            for (const key in responseData) {
              const event = {
                id: key,
                userId: userId,
                eventName: responseData[key].eventName,
                eventDate: responseData[key].eventDate,
                eventDesc: responseData[key].eventDesc,
              };
              events.push(event);
            }
        
            context.commit('setEvents', events);
          },
          async deleteEvent(context, payload){
            const userId = context.rootGetters.userId;
            const response = await fetch(
                `https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/events/${userId}/${payload.eventId}.json`,
                {
                    method: 'DELETE',
                    
                });
    
                
                //console.log(response);
                if(!response.ok){
                    //error
                }    
            },
          async editEvent(context, payload){
            const userId = context.rootGetters.userId;
            const eventData = {
                eventName: payload.eventName,
                eventDate: payload.eventDate,
                eventDesc: payload.eventDesc
            };
            const response = await fetch(
                `https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/events/${userId}/${payload.eventId}.json`,
                {
                    method: 'PUT',
                    body: JSON.stringify(eventData)
                });
    
                
                //console.log(response);
                if(!response.ok){
                    //error
                }    
                // const responseData = await response.json();
                // eventData.id = responseData.name;
                // eventData.userId = payload.userId;
                // context.commit('setEvents', eventData);
            },
          
};
