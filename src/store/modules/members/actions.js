export default {
    // transforming my data
    // this.firstName,
    // this.lastName,
    // this.userName,
    // this.email,
    // this.email,
    // this.description
    async registerMember(context, data){
        const userId = context.rootGetters.userId;
        const memberData = {
            id: context.rootGetters.userId,
            firstName: data.first,
            lastName: data.last,
            userName: data.user,
            phone: data.phone,
            description: data.desc
        };
        const token = context.rootGetters.token;
        const response = await fetch(`https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/members/${userId}.json?auth=` +
        token, 
        {
            method: 'PUT',
            body: JSON.stringify(memberData),
        }
        );
        if(!response.ok){
            //error ...
        }
        // context.commit('registerUser', memberData);
        context.commit('registerUser', {
            ...memberData,
            id: userId
        });        
    },
    async loadUser(context){
        //const token = context.getters.token;
        const userId = context.rootGetters.userId;
        const response = await fetch(
            `https://btobet-events-default-rtdb.europe-west1.firebasedatabase.app/members/${userId}.json`
            );
        
        const responseData = await response.json();
        // console.log(responseData);
        if(!response.ok) {
            // err
        }
        const members = [];

        const member = {
                id: userId,
                firstName: responseData.firstName,
                lastName: responseData.lastName,
                userName: responseData.userName,
                email: responseData.email,
                phone: responseData.phone,
                description: responseData.description
            };
            members.push(member);

        context.commit('setMember', members);
        
    },
};