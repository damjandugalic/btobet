export default {
    // members(state, _, _2, rootGetters){
    //     const userId = rootGetters.userId;
    //     return state.members.filter(user => user.id  === userId);
    // },
    members(state){
    return state.members;
    },
    hasMembers(_, getters){
        return getters.members && getters.members.length > 0;
    },
    isMember(_, getters, _2, rootGetters){ // skipping state and rootState
        const members = getters.members;
        const userId = rootGetters.userId;
        return members.some(member => member.id === userId);
    }
};        



