// https://cloud.google.com/identity-platform/docs/use-rest-api
export default {
    async login(context, payload){
        const response  = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAc699uaLllNBMIHM54FddSx1XX2s81M6g',
        {
           method: 'POST',
           body: JSON.stringify({
               email: payload.email,
               password: payload.pass,
               returnSecureToken: true
           }) 
        }); 
        
        const responseData = await response.json();
        if(!response.ok){
            //error
        }
        // console.log(responseData);
        context.commit('setUser',{
            token: responseData.idToken,
            userId: responseData.localId,
            tokenExpiration: responseData.expiresIn
        });
        
    },
    async signup(context, payload){
        const response  = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAc699uaLllNBMIHM54FddSx1XX2s81M6g',
        {
           method: 'POST',
           body: JSON.stringify({
               email: payload.email,
               password: payload.pass,
               returnSecureToken: true
           }) 
        });

        const responseData = await response.json();
        if(!response.ok){
            //error
        }

        console.log(responseData);
        context.commit('setUser',{
            token: responseData.idToken,
            userId: responseData.localId,
            tokenExpiration: responseData.expiresIn
        });
    }
};