import { createRouter, createWebHistory } from 'vue-router';

import MemberDetails from './pages/members/MemberDetails.vue';
import MemberProfile from './pages/members/MemberProfile.vue';
import UserRegistration from './pages/members/UserRegistration.vue';

import CreateEvent from './pages/events/CreateEvent.vue';
import ListEvents from './pages/events/ListEvents.vue';
import ListEvent from './pages/events/ListEvent.vue';
import NotFound from './pages/NotFound.vue';
import UserAuth from './pages/auth/UserAuth.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/members' },
        { path: '/members', component: MemberProfile },
        { 
            path: '/members/:id',
            component: MemberDetails,
            props: true, 
            children: [
                { path: 'book', component: CreateEvent, props: true }, // /members/m1/book
            ]
        },
        { path: '/register', component: UserRegistration },
        { path: '/events', component: ListEvents },
        { 
            path: '/events/:id',
            component: ListEvent,
            props: true, 
            children: [
                { path: 'event', component: CreateEvent, props: true }, // /events/e1/event
            ]
        },
        { path: '/auth', component: UserAuth},
        { path: '/:notFound(.*)', component: NotFound } 
    ]

});

export default router;